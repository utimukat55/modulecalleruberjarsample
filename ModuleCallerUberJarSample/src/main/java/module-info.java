/**
 * Module Caller Uber Jar sample
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */

module ModuleCallerUberJarSample {
	requires com.gitlab.utimukat55.thetimestring;
}