/**
 * Module Caller Uber Jar sample
 *
 * These codes are licensed under CC0.
 * http://creativecommons.org/publicdomain/zero/1.0/deed.ja
 */

package com.gitlab.utimukat55.ModuleCallerUberJarSample;

import com.gitlab.utimukat55.the_time_string.DateUtil;

/**
 * Hello world!
 *
 */
public class App {
	
	public static void main(String[] args) {
		
		System.out.println(DateUtil.hhmmssNowS());
	}
}
